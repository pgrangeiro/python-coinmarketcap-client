# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [2.5.3] - 2014-06-08
### Changed
- README.md to include new pipy lib name
- setup.py to include new pipy lib name

## [2.5.2] - 2014-06-08
### Changed
- README.md updated to include new pip install instructions

## [2.5.1] - 2014-06-08
### Changed
- setup.py file

## [2.5.0] - 2014-06-08
### Added
- Coin Market Cap Client
- CHANGELOG.md file
- CONTRIBUTING.md file

## [2.4.0] - 2014-06-08
### Added
- Coin Market Cap GlobalSummaryClient

## [2.3.0] - 2014-06-08
### Added
- Coin Market Cap CryptoCoinTickerClient


## [2.1.0] - 2014-06-08
### Added
- Coin Market Cap ListCryptoCoinClient

## [2.0.0][v2] - 2018-06-04
### Changed
- Update TickerClient to supports Coin Market Cap API v2

## [1.0.0] - 2018-06-03
### Added
- Coin Market Cap TickerClient and dependencies
- README with project details
- LICENSE file
- Initial project setup files


[2.5.3]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/master...2.5.3
[2.5.2]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.5.2...2.5.3
[2.5.1]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.5.1...2.5.2
[2.5.0]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.5.0...2.5.1
[2.2.0]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.4.0...2.5.0
[2.3.0]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.3.0...2.4.0
[2.1.0]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.1.0...2.3.0
[2.0.0]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/2.0.0...2.1.0
[1.0.0]: https://gitlab.com/pgrangeiro/python-coinmarketcap-client/compare/1.0.0...2.0.0
